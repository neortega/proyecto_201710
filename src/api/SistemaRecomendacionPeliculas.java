package api;

import java.io.*;
import java.util.Comparator;

import sun.nio.cs.ext.MSISO2022JP;
import model.data_structures.*;
import model.vo.*;
import api.ISistemaRecomendacionPeliculas;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {

	/**
	 * Constante con el separador del archivo
	 */
	public final static String SEPARADOR =",";
	/**
	 * Representa la cabeza de mi lista de peliculas VOPeliculas
	 */
	private ListaEncadenada<VOPelicula> misPeliculas;

	/**
	 * Lista que contiene los tags de todas las pel�culas
	 */
	private ListaEncadenada<VORating> ratingList;

	/**
	 * Lista que contiene los ratings de todas las pel�culas
	 */
	private ListaEncadenada<VOTag> tagList;

	/**
	 * Lista que contiene los usuarios del sistema de recomendaciones
	 */
	private ListaEncadenada<VOUsuario> userList;

	/**
	 * Pila que contiene el historial de las ultimas operaciones del usuario
	 */
	private Pila<VOOperacion> historialPila;
	
	/**
	 * Lista que guarda los generos de las pel�culas
	 */
	private ListaEncadenada<VOGeneroPelicula> generosList;
<<<<<<< HEAD
	
	/**
	 * Lista que guarda los generos a los que un usuario ha realizado tags o ratings
	 */
	private ListaEncadenada<VOGeneroUsuario> genUserList;
	
	
	/**
	 * Lista que guarda una cantidad de g�neros que contiene usuarios a los que tiene asociados
	 */
=======

	private ListaEncadenada<VOGeneroUsuario> genUserList;

>>>>>>> 3d6225f4e80e68bd829286d70669fa3285db1efb
	private ListaEncadenada<VOUsuarioGenero> userGenList;
	
	/**
	 * Comparador de pel�culas
	 */
	private MergeList <VOPelicula>comparador;
	
	/**
	 * Comparador de ratings 
	 */
	private MergeList <VORating> comparador2;
<<<<<<< HEAD
	
	/**
	 * Comparador de usuarios por cantidad de ratings
	 */
	private MergeList<VOUsuarioConteo> comparador3;
	
	/**
	 * Comparador de usuarios
	 */
=======


	private MergeList<VOUsuarioConteo> comparador3;

>>>>>>> 3d6225f4e80e68bd829286d70669fa3285db1efb
	private MergeList<VOUsuario> comparador4;

	/**
	 * Comparador de tags
	 */
	private MergeList<VOTag> comparador5;
	
	/**
	 * Constructor del sistema de recomendaci�n de pel�culas
	 */
	public SistemaRecomendacionPeliculas()
	{
		misPeliculas = new ListaEncadenada<>();
		ratingList = new ListaEncadenada<>();
		tagList = new ListaEncadenada<>();
		userList = new ListaEncadenada<>();
		generosList = new ListaEncadenada<>();
		genUserList = new ListaEncadenada<>();
		userGenList = new ListaEncadenada<>();
		historialPila = new Pila<VOOperacion>();
		comparador = new MergeList<VOPelicula>();
		comparador2 = new MergeList<VORating>();
		comparador3 = new MergeList<VOUsuarioConteo>();
		comparador4 = new MergeList<VOUsuario>();
		comparador5 = new MergeList<VOTag>();
	}

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) throws Exception {
		// TODO Auto-generated method stub
		System.currentTimeMillis();
		boolean seCargo = false;
		String generosPeliculas = "";
		try {
			BufferedReader read = new BufferedReader(new FileReader(rutaPeliculas));
			String line = read.readLine();
			while(line!=null)
			{
				String[] partes = line.split(SEPARADOR);
				if(partes.length>3)
				{
					for (int i = 1; i < partes.length-1; i++) {
						partes[1] += partes[i];
					}
				}
				VOPelicula nuevaPelicula= new VOPelicula();
				int agno = Integer.parseInt(partes[1].substring(partes[1].length()-5, partes[1].length()-2));
				String titulo = partes[1].substring(0,partes[1].length()-8);
				nuevaPelicula.setAgnoPublicacion(agno);
				nuevaPelicula.setTitulo(titulo.replace('"', ' '));
				ILista<String> generos = new ListaEncadenada<String>();
				String[] gen = partes[3].split("|");
				for (int i = 0; i < gen.length; i++) {
					generos.agregarElementoFinal(gen[i]);
					if(!generosPeliculas.contains(gen[i]))
					{
						generosPeliculas += gen[i] + ",";
					}
				}
				cargarPorGenero(generosPeliculas);
				nuevaPelicula.setGenerosAsociados(generos);
				misPeliculas.agregarElementoFinal(nuevaPelicula);
			}
			read.close();
			seCargo = true;
		} catch (Exception e) {
			throw new Exception("Error al cargar la informacion del archivo ingresado");
		}
		System.currentTimeMillis();
		return seCargo;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		// TODO Auto-generated method stub
		System.currentTimeMillis();
		boolean seCargo = false;
		try
		{
			File archivo = new File(rutaRatings);
			FileReader fr = new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);

			String linea = br.readLine();
			while(linea != null)
			{
				String[] ratings = linea.split(SEPARADOR);

				VORating vor = new VORating();
				vor.setIdUsuario(Long.parseLong(ratings[0]));
				vor.setIdPelicula(Long.parseLong(ratings[1]));
				vor.setRating(Double.valueOf(ratings[2]));
				vor.setTimesTamp(Long.parseLong(ratings[3]));

				ratingList.agregarElementoFinal(vor);

				VOUsuario vou = new VOUsuario();
				vou.setIdUsuario(Long.parseLong(ratings[0]));
				if(userList.darCabeza() == null)
					userList.agregarElementoFinal(vou);
				else
				{
					NodoSencillo<VOUsuario> ref = userList.darCabeza();
					while(ref != null)
					{
						NodoSencillo<VOUsuario> sig = ref.darSiguiente();
						if(vou.getIdUsuario() != ref.darObjeto().getIdUsuario() || vou.getIdUsuario() != sig.darObjeto().getIdUsuario())
						{
							userList.agregarElementoFinal(vou);
						}
						ref = ref.darSiguiente();
					}
				}
				if(generosList.darNumeroElementos()!=0)
				{
					//Recorro todos los generos que hay
					for(int i = 0; i < generosList.darNumeroElementos(); i++)
					{
						VOGeneroUsuario vogu = new VOGeneroUsuario();
						//Le agrego el g�nero de la pel�cula actual al genero usuario
						vogu.setGenero(generosList.darElemento(i).getGenero());

						if(userList.darNumeroElementos()!=0 && ratingList.darNumeroElementos()!=0)
						{
							//Creo una lista de usuarios conteo
							ILista<VOUsuarioConteo> voucList = new ListaEncadenada<VOUsuarioConteo>();
							ListaEncadenada<VOPelicula> vop = (ListaEncadenada<VOPelicula>)generosList.darElemento(i).getPeliculas();
							//Recorro toda la lista de usuarios, obtengo la lista peliculas que tengan asociada el g�nero
							for(int j = 0; j < userList.darNumeroElementos(); i++)
							{
								//Comparo si el rating de la posici�n actual tiene la misma pel�cula y usuario, si es as� 
								//tengo un usuario que agreg� un rating a una pel�cula con cierto g�nero
								if(ratingList.darElemento(j).getIdPelicula() == vop.darElemento(j).getIdUsuario())
								{
									if(ratingList.darElemento(j).getIdUsuario() == userList.darElemento(j).getIdUsuario())
									{
										VOUsuarioConteo vouc = new VOUsuarioConteo();
										vouc.setIdUsuario(userList.darElemento(j).getIdUsuario());
										vouc.setConteo(userList.darElemento(j).getNumRatings());
									}
								}
							}
							//Agrego los usuarios que hayan colocado un rating a una pel�cula con el g�nero dicho
							vogu.setUsuarios(voucList);
						}
						//Agrego los usuarios a la lista temporal
						genUserList.agregarElementoFinal(vogu);
					}
				}
				if(vor.getIdUsuario() == vou.getIdUsuario())
				{
					int numRatings = vou.getNumRatings();
					numRatings ++;
					vou.setNumRatings(numRatings);
				}
				br.readLine();
			}
			seCargo = true;
			br.close();
			fr.close();
		}
		catch(IOException e)
		{
			seCargo = false;
		}
		System.currentTimeMillis();
		return seCargo;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		// TODO Auto-generated method stub
		System.currentTimeMillis();
		boolean seCargo = false;
		try
		{
			File archivo = new File(rutaTags);
			FileReader fr = new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);

			String linea = br.readLine();
			while(linea != null)
			{
				String[] tags = linea.split(SEPARADOR);

				String tagsAux = "";
				for(int i = 2; i < tags.length-1; i++)
				{
					tagsAux.concat(tags[i]+"");
				}

				VOTag vot = new VOTag();
				vot.setTag(tagsAux);
				vot.setTimestamp(Long.parseLong(tags[tags.length]));
				tagList.agregarElementoFinal(vot);

				VOUsuario vou = new VOUsuario();
				vou.setIdUsuario(Long.parseLong(tags[0]));

				if(userList.darCabeza() == null)
					userList.agregarElementoFinal(vou);

				else
				{
					NodoSencillo<VOUsuario> ref = userList.darCabeza();
					while(ref != null)
					{
						NodoSencillo<VOUsuario> sig = ref.darSiguiente();
						if(vou.getIdUsuario() != ref.darObjeto().getIdUsuario() || vou.getIdUsuario() != sig.darObjeto().getIdUsuario())
						{
							userList.agregarElementoFinal(vou);
						}
						ref = ref.darSiguiente();
					}
				}

				if(misPeliculas.darNumeroElementos() != 0)
				{
					ILista<String> tagsAsocList = new ListaEncadenada<String>();
					VOPelicula vop = new VOPelicula();
					for(int i = 0; i < misPeliculas.darNumeroElementos(); i++)
					{
						vop = misPeliculas.darElemento(i);
						if(vop.getIdUsuario() == Long.parseLong(tags[1]))
						{
							String tagsAsoc = tagsAux;
							tagsAsocList.agregarElementoFinal(tagsAsoc);
						}
					}
					vop.setTagsAsociados(tagsAsocList);
					vop.setNumeroTags(tagsAsocList.darNumeroElementos());
				}

				br.readLine();
			}
			seCargo = true;
			br.close();
			fr.close();
		}
		catch(IOException ioe)
		{
			seCargo = false;
		}
		System.currentTimeMillis();
		return seCargo;
	}

	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		System.currentTimeMillis();
		VOOperacion nuevaOperacion = new VOOperacion();
		nuevaOperacion.setOperacion("sizeMovies");
		historialPila.push(nuevaOperacion);
		System.currentTimeMillis();
		return misPeliculas.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		//guarda la operacion para el punto C
		System.currentTimeMillis();
		VOOperacion nuevaOperacion = new VOOperacion();
		nuevaOperacion.setOperacion("sizeUsers");
		historialPila.push(nuevaOperacion);
		System.currentTimeMillis();
		return userList.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		//guarda la operacion para el punto C
		System.currentTimeMillis();
		VOOperacion nuevaOperacion = new VOOperacion();
		nuevaOperacion.setOperacion("sizeTags");
		historialPila.push(nuevaOperacion);
		System.currentTimeMillis();
		return tagList.darNumeroElementos();
	}

	@Override
<<<<<<< HEAD
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		System.currentTimeMillis();
		int referenciaDelParametro = 0;
=======
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n){
		//guarda la operacion para el punto C
		VOOperacion nuevaOperacion = new VOOperacion();
		nuevaOperacion.setTimestampInicio(System.currentTimeMillis());
		//empieza metodo
		//empieza recorriendo la lista de generos la cual ya esta ordenada por rating
>>>>>>> b801edf98b6dd93fa6dffd9a8fb15fea7fc17d90
		ListaEncadenada<VOGeneroPelicula> respuesta = generosList;
		while(respuesta.avanzarSiguientePosicion())
		{
			// entro a la lista de peliculas del genero actual

			ListaEncadenada<VOPelicula> peliculasPorGenero = (ListaEncadenada<VOPelicula>) respuesta.darElementoPosicionActual().getPeliculas();
			if(n<=peliculasPorGenero.darNumeroElementos())
			{
				peliculasPorGenero.eliminarObjeto(n-1);
			}
			respuesta.avanzarSiguientePosicion();
		}
		//continuacion operacion punto c
		nuevaOperacion.setTimestampFin(System.currentTimeMillis());
		nuevaOperacion.setOperacion("peliculasPopulares");
		historialPila.push(nuevaOperacion);

		return respuesta;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		//guarda la operacion para el punto C
		VOOperacion nuevaOperacion = new VOOperacion();
		nuevaOperacion.setTimestampInicio(System.currentTimeMillis());
		//empieza metodo
		Comparator <VOPelicula>c = new Comparator<VOPelicula>() {
			//crea el objeto de tipo Comparator para el merge
			public int compare(VOPelicula a, VOPelicula b) {
				int respuesta = 0;
				if(a.getAgnoPublicacion()>b.getAgnoPublicacion())
				{
					respuesta = 1;
				}
				else if(a.getAgnoPublicacion()<b.getAgnoPublicacion())
				{
					respuesta =-1;
				}
				else if(a.getAgnoPublicacion()==b.getAgnoPublicacion())
				{
					if(a.getNumeroRatings()>b.getNumeroRatings())
					{
						respuesta = 1;
					}
					else if(a.getNumeroRatings()<b.getNumeroRatings())
					{
						respuesta = -1;
					}
					else if(a.getNumeroRatings()==b.getNumeroRatings())
					{
						if(a.getTitulo().compareToIgnoreCase(b.getTitulo())>0)
						{
							respuesta = 1;
						}
						else
						{
							respuesta =-1;
						}
					}
				}
				return respuesta;
			}
		};
		comparador.merge_sort(misPeliculas.darCabeza(), c);
		//Continuacion operacion punto C
		nuevaOperacion.setTimestampFin(System.currentTimeMillis());
		nuevaOperacion.setOperacion("catalogoPeliculasOrdenado");
		historialPila.push(nuevaOperacion);

		System.currentTimeMillis();
		
		return misPeliculas;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
<<<<<<< HEAD
		System.currentTimeMillis();
=======
		//guarda la operacion para el punto C
		VOOperacion nuevaOperacion = new VOOperacion();
		nuevaOperacion.setOperacion("recomendarGenero");
		nuevaOperacion.setTimestampInicio(System.currentTimeMillis());
		//empieza metodo
>>>>>>> b801edf98b6dd93fa6dffd9a8fb15fea7fc17d90
		ListaEncadenada<VOGeneroPelicula> respuesta = generosList;
		while(respuesta.avanzarSiguientePosicion())
		{
			ListaEncadenada<VOPelicula> peliculasPorGenero = (ListaEncadenada<VOPelicula>)respuesta.darElementoPosicionActual().getPeliculas();
			peliculasPorGenero.darCabeza().modificarSiguiente(null);
			respuesta.avanzarSiguientePosicion();
		}
		// Continuacion operacion punto C
		nuevaOperacion.setTimestampFin(System.currentTimeMillis());
		historialPila.push(nuevaOperacion);

		System.currentTimeMillis();
		
		return respuesta;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
<<<<<<< HEAD
		System.currentTimeMillis();
=======
		//guarda la operacion para el punto C
		VOOperacion nuevaOperacion = new VOOperacion();
		nuevaOperacion.setTimestampInicio(System.currentTimeMillis());
		nuevaOperacion.setOperacion("opinionRatingsGenero");
		//empieza el metodo 
>>>>>>> b801edf98b6dd93fa6dffd9a8fb15fea7fc17d90
		ListaEncadenada<VOGeneroPelicula> respuesta = generosList;
		while(respuesta.avanzarSiguientePosicion())
		{
			ListaEncadenada<VOPelicula> listaPeliculasGenero = (ListaEncadenada<VOPelicula>)respuesta.darElementoPosicionActual().getPeliculas();
			comparador.merge_sort(listaPeliculasGenero.darCabeza(), new Comparator<VOPelicula>() {

				//crea el comparador que usara el merge
				public int compare(VOPelicula a, VOPelicula b) {
					int respuesta =-1;
					if(a.getAgnoPublicacion()>b.getAgnoPublicacion())
					{
						respuesta = 1;
					}
					return respuesta;
				}
			});
			respuesta.avanzarSiguientePosicion();
		}
		//Continuacion operacion punto c
		nuevaOperacion.setTimestampFin(System.currentTimeMillis());
		historialPila.push(nuevaOperacion);
		System.currentTimeMillis();
		return respuesta;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(
			String rutaRecomendacion, int n) {
<<<<<<< HEAD
		System.currentTimeMillis();
=======
		//guarda la operacion para el punto C

		VOOperacion nuevaOperacion = new VOOperacion();
		nuevaOperacion.setOperacion("recomendarPeliculas");
		nuevaOperacion.setTimestampInicio(System.currentTimeMillis());

		//empieza el metodo

>>>>>>> b801edf98b6dd93fa6dffd9a8fb15fea7fc17d90
		ListaEncadenada<VOPeliculaPelicula> respuesta = new ListaEncadenada<VOPeliculaPelicula>();
		int contador =0;
		try
		{
			File archivo = new File(rutaRecomendacion);
			FileReader fr = new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);

			String linea = br.readLine();
			while(linea != null)
			{
				VOPeliculaPelicula objeto = new VOPeliculaPelicula();

				String[] peliculasYcalificacion = linea.split(SEPARADOR);

				long idPelicula = Long.parseLong(peliculasYcalificacion[1]);
				double calificacion = Double.parseDouble(peliculasYcalificacion[2]);
				String generoPrincipal = "";

				ListaEncadenada<VOPelicula> refMisPeliculas = misPeliculas;
				//busco en peliculas la pelicula con ese id
				while (refMisPeliculas.avanzarSiguientePosicion())
				{
					if(refMisPeliculas.darElementoPosicionActual().getIdUsuario()== idPelicula)
					{
						//Guardo su genero
						generoPrincipal = refMisPeliculas.darElementoPosicionActual().getGenerosAsociados().darElementoPosicionActual();
						objeto.setPelicula(refMisPeliculas.darElementoPosicionActual());
						break;
					}
					refMisPeliculas.avanzarSiguientePosicion();		
				}  
				//Ahora recorro la de generos, buscando la lista con el genero principal de la pelicula
				ListaEncadenada<VOGeneroPelicula> refgeneroList = generosList;

				while (refgeneroList.avanzarSiguientePosicion())
				{
					if(refgeneroList.darElementoPosicionActual().getGenero()== generoPrincipal)
					{
						//referencia de la lista 
						ListaEncadenada<VOPelicula> listaDeGeneroBuscado = (ListaEncadenada<VOPelicula>)refgeneroList.darElementoPosicionActual().getPeliculas();
						// lista con las peliculas relacionadas
						ListaEncadenada<VOPelicula> peliculasRelacionadas = new ListaEncadenada<VOPelicula>();

						while (listaDeGeneroBuscado.avanzarSiguientePosicion() && contador<=n)
						{
							VOPelicula objetoActual = listaDeGeneroBuscado.darElementoPosicionActual();
							if(objetoActual.getPromedioRatings()< calificacion+0.5 && objetoActual.getPromedioRatings()> calificacion-0.5)
							{
								peliculasRelacionadas.agregarElementoFinal(objetoActual);
							}
							listaDeGeneroBuscado.avanzarSiguientePosicion();
							contador++;
						}
						comparador.merge_sort(peliculasRelacionadas.darCabeza(), new Comparator<VOPelicula>() {

							//crea el comparador que usara el merge
							public int compare(VOPelicula a, VOPelicula b) {
								int respuesta =-1;
								if(a.getAgnoPublicacion()>b.getAgnoPublicacion())
								{
									respuesta = 1;
								}
								return respuesta;
							}
						});
						objeto.setPeliculasRelacionadas(peliculasRelacionadas);
					}
				}
				respuesta.agregarElementoFinal(objeto);
				br.readLine();
			}
			br.close();
			fr.close();
		}
		catch(IOException ioe)
		{
		}
		//Continuacion operacion punto C
		nuevaOperacion.setTimestampFin(System.currentTimeMillis());
		historialPila.push(nuevaOperacion);
		System.currentTimeMillis();
		return respuesta;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {

<<<<<<< HEAD
		System.currentTimeMillis();
=======
		//guarda la operacion para el punto C
		VOOperacion nuevaOperacion = new VOOperacion();
		nuevaOperacion.setOperacion("ratingsPelicula");
		nuevaOperacion.setTimestampInicio(System.currentTimeMillis());

		//termina el comando del punto C
>>>>>>> b801edf98b6dd93fa6dffd9a8fb15fea7fc17d90
		ListaEncadenada<VORating> respuesta = new ListaEncadenada<VORating>();

		ListaEncadenada<VORating> refListaRating = ratingList;
		//busca en la listo todos los ratings con el id ingresado por parametro
		while(refListaRating.avanzarSiguientePosicion())
		{
			if(refListaRating.darElementoPosicionActual().getIdPelicula()== idPelicula)
			{
				respuesta.agregarElementoFinal(refListaRating.darElementoPosicionActual());
			}
			refListaRating.avanzarSiguientePosicion();
		}

		comparador2.merge_sort(respuesta.darCabeza(), new Comparator<VORating>() {

			@Override
			public int compare(VORating a, VORating b) {
				int respuesta = -1;
				if(a.getTimesTamp()>b.getTimesTamp()){
					respuesta = 1;
				}
				return respuesta;
			}
		});

		//continuacion proceso para el punto c
		nuevaOperacion.setTimestampFin(System.currentTimeMillis());
		historialPila.push(nuevaOperacion);
		System.currentTimeMillis();
		return respuesta;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		// TODO Auto-generated method stub
		//Lista respuesta
<<<<<<< HEAD
		System.currentTimeMillis();
				ILista<VOGeneroUsuario> listRespuesta = new ListaEncadenada<VOGeneroUsuario>();
				
				Comparator<VOUsuarioConteo> c = new Comparator<VOUsuarioConteo>(){

					@Override
					public int compare(VOUsuarioConteo arg0,
							VOUsuarioConteo arg1) {
						// TODO Auto-generated method stub
						if(arg0.getConteo() == arg1.getConteo())
							return 0;
						else if(arg0.getConteo() > arg1.getConteo())
							return 1;
						else
							return -1;
					}
				};
				
				//Recorro la lista temporal, obtengo los usuarios por g�nero, los ordeno, recorro hasta n y dejo la lista
				//con la cantidad n de usuarios, y los usuarios ordenados.
				for(int i = 0; i < genUserList.darNumeroElementos(); i++)
				{
						
					VOGeneroUsuario vogu2 = new VOGeneroUsuario();
					vogu2 = genUserList.darElemento(i);
						
					ListaEncadenada<VOUsuarioConteo> vocont = (ListaEncadenada<VOUsuarioConteo>)vogu2.getUsuarios();
					NodoSencillo<VOUsuarioConteo> cabeza = vocont.darCabeza();
						
					comparador3.merge_sort(cabeza, c);
						
					listRespuesta.agregarElementoFinal(vogu2);
					if(i == n)
						break;
				}
				
				//Retorno la lista de respuesta.
				System.currentTimeMillis();
				return listRespuesta;
=======
		ILista<VOGeneroUsuario> listRespuesta = new ListaEncadenada<VOGeneroUsuario>();

		Comparator<VOUsuarioConteo> c = new Comparator<VOUsuarioConteo>(){

			@Override
			public int compare(VOUsuarioConteo arg0,
					VOUsuarioConteo arg1) {
				// TODO Auto-generated method stub
				if(arg0.getConteo() == arg1.getConteo())
					return 0;
				else if(arg0.getConteo() > arg1.getConteo())
					return 1;
				else
					return -1;
			}
		};

		//Recorro la lista temporal, obtengo los usuarios por g�nero, los ordeno, recorro hasta n y dejo la lista
		//con la cantidad n de usuarios, y los usuarios ordenados.
		for(int i = 0; i < genUserList.darNumeroElementos(); i++)
		{

			VOGeneroUsuario vogu2 = new VOGeneroUsuario();
			vogu2 = genUserList.darElemento(i);

			ListaEncadenada<VOUsuarioConteo> vocont = (ListaEncadenada<VOUsuarioConteo>)vogu2.getUsuarios();
			NodoSencillo<VOUsuarioConteo> cabeza = vocont.darCabeza();

			comparador3.merge_sort(cabeza, c);

			listRespuesta.agregarElementoFinal(vogu2);
			if(i == n)
				break;
		}

		//Retorno la lista de respuesta.
		return listRespuesta;
>>>>>>> 3d6225f4e80e68bd829286d70669fa3285db1efb
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		// TODO Auto-generated method stub
		System.currentTimeMillis();
		Comparator<VOUsuario> c = new Comparator<VOUsuario>()
				{

			@Override
			public int compare(VOUsuario arg0, VOUsuario arg1) {
				// TODO Auto-generated method stub
				int respuesta = 0;

				if(arg0.getPrimerTimestamp() > arg1.getPrimerTimestamp())
					respuesta = 1;
				else if(arg0.getPrimerTimestamp() < arg1.getPrimerTimestamp())
					respuesta = -1;
				else
				{
					if(arg0.getNumRatings() > arg1.getNumRatings())
						respuesta = 1;
					else if(arg0.getNumRatings() < arg1.getNumRatings())
						respuesta = -1;
					else
					{
						if(arg0.getIdUsuario() > arg1.getIdUsuario())
							respuesta = 1;
						else if(arg0.getIdUsuario() < arg1.getIdUsuario())
							respuesta = -1;
					}
				}

				return respuesta;
			}

				};
<<<<<<< HEAD
			if(userList.darNumeroElementos()!=0)	
				comparador4.merge_sort(userList.darCabeza(), c);
		
			System.currentTimeMillis();
			
		return userList;
=======
				if(userList.darNumeroElementos()!=0)	
					comparador4.merge_sort(userList.darCabeza(), c);

				return userList;
>>>>>>> 3d6225f4e80e68bd829286d70669fa3285db1efb
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		// TODO Auto-generated method stub
		System.currentTimeMillis();
		ILista<VOGeneroPelicula> respuesta = generosList;
<<<<<<< HEAD
		
		
=======

>>>>>>> 3d6225f4e80e68bd829286d70669fa3285db1efb
		Comparator<VOPelicula> c = new Comparator<VOPelicula>()
				{

			@Override
			public int compare(VOPelicula arg0, VOPelicula arg1) {
				// TODO Auto-generated method stub
				if(arg0.getNumeroTags() == arg1.getNumeroTags())
					return 0;
				else if(arg0.getNumeroTags() > arg0.getNumeroTags())
					return 1;
				else
					return -1;
			}
				};

				ListaEncadenada<VOPelicula> listaPeliculasGeneros = new ListaEncadenada<VOPelicula>();
				if(generosList.darNumeroElementos() != 0)
				{
					ListaEncadenada<VOPelicula> temp = new ListaEncadenada<VOPelicula>();
					for(int i = 0; i < generosList.darNumeroElementos(); i++)
					{
						VOGeneroPelicula vogp = generosList.darElemento(i);

						listaPeliculasGeneros = (ListaEncadenada<VOPelicula>)vogp.getPeliculas();
						comparador.merge_sort(listaPeliculasGeneros.darCabeza(), c);
						for(int j = 0; j <= n; j++)
						{
							temp.agregarElementoFinal(listaPeliculasGeneros.darElemento(i));
						}
						vogp.setPeliculas(temp);
					}
				}
<<<<<<< HEAD
				vogp.setPeliculas(temp);
			}
		}
		System.currentTimeMillis();
		
		return respuesta;
=======
				return respuesta;
>>>>>>> 3d6225f4e80e68bd829286d70669fa3285db1efb
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(
			String rutaRecomendacion, int n) {
		//guarda la operacion para el punto C

		VOOperacion nuevaOperacion = new VOOperacion();
		nuevaOperacion.setOperacion("recomendarPeliculas");
		nuevaOperacion.setTimestampInicio(System.currentTimeMillis());

		//empieza el metodo

		ListaEncadenada<VOPeliculaUsuario> respuesta = new ListaEncadenada<VOPeliculaUsuario>();
		int contador =0;
		try
		{
			File archivo = new File(rutaRecomendacion);
			FileReader fr = new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);

			String linea = br.readLine();
			while(linea != null)
			{
				VOPeliculaUsuario objeto = new VOPeliculaUsuario();

				String[] peliculasYcalificacion = linea.split(SEPARADOR);

				long idPelicula = Long.parseLong(peliculasYcalificacion[1]);
				double calificacion = Double.parseDouble(peliculasYcalificacion[2]);
				String idusuarios = "";

				ListaEncadenada<VORating> refRatingList = ratingList;
				//busco en peliculas la pelicula con ese id
				while (refRatingList.avanzarSiguientePosicion())
				{
					if(refRatingList.darElementoPosicionActual().getIdPelicula()== idPelicula && refRatingList.darElementoPosicionActual().getRating()>= calificacion && refRatingList.darElementoPosicionActual().getRating()>= calificacion)
					{
						idusuarios += refRatingList.darElementoPosicionActual().getIdUsuario() + ",";
					}
					refRatingList.avanzarSiguientePosicion();		
				}  
				//guardo la lista con los usuariosAsosiados
				ListaEncadenada<VOUsuario> usuarios = new ListaEncadenada<VOUsuario>();
				//Ahora recorro la de usuarios, buscando los que tienen el id 
				ListaEncadenada<VOUsuario> refUsuarioList = userList;
				while (refUsuarioList.avanzarSiguientePosicion() && contador<=n)
				{
					if(idusuarios.contains(String.valueOf(refUsuarioList.darElementoPosicionActual().getIdUsuario())))
					{
						usuarios.agregarElementoFinal(refUsuarioList.darElementoPosicionActual());
					}
					refUsuarioList.avanzarSiguientePosicion();
					contador++;
				}
				objeto.setUsuariosRecomendados(usuarios);

				respuesta.agregarElementoFinal(objeto);
				br.readLine();
			}
			br.close();
			fr.close();
		}
		catch(IOException ioe)
		{
		}
		//Continuacion operacion punto C
		nuevaOperacion.setTimestampFin(System.currentTimeMillis());
		historialPila.push(nuevaOperacion);
		return respuesta;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub
		System.currentTimeMillis();
		ListaEncadenada<VOTag> votList = new ListaEncadenada<VOTag>();
		
		Comparator<VOTag> c = new Comparator<VOTag>()
				{

					@Override
					public int compare(VOTag arg0, VOTag arg1) {
						// TODO Auto-generated method stub
						int respuesta = 0;
						
						if(arg0.getTimestamp() == arg1.getTimestamp())
							respuesta = 0;
						if(arg0.getTimestamp() > arg1.getTimestamp())
							respuesta = 1;
						if(arg0.getTimestamp() < arg1.getTimestamp())
							respuesta = -1;
						
						return respuesta;
					}
				};
				
				ListaEncadenada<VOTag> temp = new ListaEncadenada<VOTag>();
				
				if(misPeliculas.darNumeroElementos() != 0)
				{
					for(int i = 0; i < misPeliculas.darNumeroElementos(); i++)
					{
						VOPelicula vop = misPeliculas.darElemento(i);
						
						ListaEncadenada<String> tagstemp = (ListaEncadenada<String>)vop.getTagsAsociados();
						for(int j = 0; j < tagList.darNumeroElementos(); j++)
						{
							if(tagList.darElemento(j).getTag().equals(tagstemp.darElemento(j)))
							{
								VOTag vot = new VOTag();
								vot.setTag(tagstemp.darElemento(j));
								vot.setTimestamp(tagList.darElemento(j).getTimestamp());
								temp.agregarElementoFinal(vot);
							}
						}
					}
				}
				comparador5.merge_sort(temp.darCabeza(), c);
				votList = temp;
				System.currentTimeMillis();
		return votList;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		ListaEncadenada<VOOperacion> respuesta = new ListaEncadenada<>();
		while(!historialPila.isEmpty())
		{
			respuesta.agregarElementoFinal(historialPila.pop());

		}
		return respuesta;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {

		while(!historialPila.isEmpty())
		{
			historialPila.pop();
		}

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		System.currentTimeMillis();
		ListaEncadenada<VOOperacion> respuesta = new ListaEncadenada<>();
		int contador = 0;
		while(contador<=n)
		{
			respuesta.agregarElementoFinal(historialPila.pop());
			contador++;
		}
		System.currentTimeMillis();
		return respuesta;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		System.currentTimeMillis();
		int contador = 0;
		while(contador<=n)
		{
			historialPila.pop();
			contador++;
		}
		System.currentTimeMillis();
		// TODO Auto-generated method stub
	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub

	}
	public void cargarPorGenero (String generos)
	{
		String[] generosDisponibles = generos.split(",");
		for (int i = 0; i < generosDisponibles.length; i++) {
			VOGeneroPelicula nuevoElemento = new VOGeneroPelicula();
			nuevoElemento.setGenero(generosDisponibles[i]);
			generosList.agregarElementoFinal(nuevoElemento);
		}
		NodoSencillo<VOGeneroPelicula> cabezaGeneros =generosList.darCabeza();
		while(cabezaGeneros!=null)
		{
			String generoRef= cabezaGeneros.darObjeto().getGenero();
			NodoSencillo<VOPelicula> cabeza =misPeliculas.darCabeza();
			while(cabeza.darSiguiente()!=null)
			{
				ListaEncadenada<String> generoDePelicula =(ListaEncadenada<String>)cabeza.darObjeto().getGenerosAsociados();
				while (generoDePelicula!=null) {
					if(generoRef.equals(generoDePelicula.darElementoPosicionActual()))
					{
						generosList.darElementoPosicionActual().getPeliculas().agregarElementoFinal(cabeza.darObjeto());
					}
					generoDePelicula.avanzarSiguientePosicion();	
				}
				cabeza = cabeza.darSiguiente();
			}
			ListaEncadenada<VOPelicula> totalPeliculasPorGenero = (ListaEncadenada<VOPelicula>)cabezaGeneros.darObjeto().getPeliculas();
			comparador.merge_sort(totalPeliculasPorGenero.darCabeza(), new Comparator<VOPelicula>() {
				@Override
				public int compare(VOPelicula a, VOPelicula b) {
					int respuesta = 0;
					if(a.getNumeroRatings()>b.getNumeroRatings())
					{
						respuesta = 1;
					}
					else if (a.getNumeroRatings()<b.getNumeroRatings())
					{
						respuesta = -1;
					}
					else if (a.getNumeroRatings()==b.getNumeroRatings())
					{
						respuesta = 1;
					}
					return 	respuesta;
				}
			});
			cabezaGeneros = cabezaGeneros.darSiguiente();
		}
	}
}


