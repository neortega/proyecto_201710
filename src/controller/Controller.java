package controller;

import api.*;
import model.data_structures.*;
import model.vo.*;

public class Controller {
	
	private static ISistemaRecomendacionPeliculas srp = new SistemaRecomendacionPeliculas();
	
	public static void cargarPeliculas()
	{
		try
		{
			srp.cargarPeliculasSR("./data/movies.csv");
			srp.cargarRatingsSR("./data/ratings.csv");
			srp.cargarTagsSR("./data/tags.csv");
		}
		catch(Exception ioe)
		{
			//
		}
	}
	
	public static int sizeMoviesSR()
	{
		return srp.sizeMoviesSR();
	}
	public static int sizeUsersSR()
	{
		return srp.sizeUsersSR();
	}
	public static int sizeTagsSR()
	{
		return srp.sizeMoviesSR();
	}
	public static ListaEncadenada<VOGeneroPelicula> peliculasPopularesSR(int n)
	{
		return (ListaEncadenada<VOGeneroPelicula>) srp.peliculasPopularesSR(n);
	}
	public static ListaEncadenada<VOPelicula> catalogoPeliculasOrdenadoSR()
	{
		return (ListaEncadenada<VOPelicula>) srp.catalogoPeliculasOrdenadoSR();
	}
	public static ListaEncadenada<VOGeneroPelicula> recomendarGeneroSR()
	{
		return (ListaEncadenada<VOGeneroPelicula>) srp.recomendarGeneroSR();
	}
	public static ListaEncadenada<VOGeneroPelicula>opinionRatingsGeneroSR()
	{
		return (ListaEncadenada<VOGeneroPelicula>) srp.opinionRatingsGeneroSR();
	}
	public static ListaEncadenada<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n)
	{
		return (ListaEncadenada<VOPeliculaPelicula>) srp.recomendarPeliculasSR(rutaRecomendacion, n);
	}
	public static ListaEncadenada<VORating> ratingsPeliculaSR (long idPelicula)
	{
		return (ListaEncadenada<VORating>) srp.ratingsPeliculaSR(idPelicula);
	}
	public static ListaEncadenada<VOGeneroUsuario> usuariosActivosSR(int n)
	{
		return (ListaEncadenada<VOGeneroUsuario>) srp.usuariosActivosSR(n);
	}
	public static ListaEncadenada<VOUsuario> catalogoUsuariosOrdenadoSR()
	{
		return (ListaEncadenada<VOUsuario>) srp.catalogoUsuariosOrdenadoSR();
	}
	public static ListaEncadenada<VOGeneroPelicula> recomendarTagsGeneroSR(int n)
	{
		return (ListaEncadenada<VOGeneroPelicula>) srp.recomendarTagsGeneroSR(n);
	}
	public static ListaEncadenada<VOUsuarioGenero> opinionTagsGeneroSR()
	{
		return (ListaEncadenada<VOUsuarioGenero>) srp.opinionTagsGeneroSR();
	}
	public static ListaEncadenada<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n)
	{
		return (ListaEncadenada<VOPeliculaUsuario>) srp.recomendarUsuariosSR(rutaRecomendacion, n);
	}
	public static ListaEncadenada<VOTag> tagsPeliculaSR(int idPelicula)
	{
		return (ListaEncadenada<VOTag>) srp.tagsPeliculaSR(idPelicula);
	}
	public static void agregarOperacionSR(String idOperacion,long tInicio, long tFin)
	{
		srp.agregarOperacionSR(idOperacion, tInicio, tFin);
	}
	public static ListaEncadenada<VOOperacion> darHistoralOperacionesSR()
	{
		return (ListaEncadenada<VOOperacion>) srp.darHistoralOperacionesSR();
	}
	public static void  limpiarHistorialOperacionesSR()
	{
		srp.limpiarHistorialOperacionesSR();
	}
	public static ListaEncadenada<VOOperacion> darUltimasOperaciones(int n)
	{
		return (ListaEncadenada<VOOperacion>) srp.darUltimasOperaciones(n);
	}
	public static void borrarUltimasOperaciones(int n)
	{
		srp.borrarUltimasOperaciones(n);
	}
	public static long agregarPelicula(String titulo, int agno, String[] generos)
	{
		return srp.agregarPelicula(titulo, agno, generos);
	}
	public static void agregarRating(int idUsuario, int idPelicula, double rating)
	{
		srp.agregarRating(idUsuario, idPelicula, rating);
	}
}
