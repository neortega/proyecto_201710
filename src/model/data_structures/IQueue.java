package model.data_structures;

public interface IQueue<T> {

	/**
	 * Agrega un elemento al final de la cola
	 * @param item - Item que se agregar� al final de la cola
	 */
	public void enqueue(T item);
	
	/**
	 * Elimina el elemento que est� al final de la cola
	 */
	public T dequeue();
	
	/**
	 * Indica si la cola est� vac�a o no
	 * @return true si la cola no tiene ning�n elemento
	 * @return false si la cola tiene uno o m�s elementos
	 */
	public boolean isEmpty();
	
	/**
	 * N�mero de elementos en la cola
	 * @return Cantidad de elementos que tiene la cola
	 */
	public int size();
}
