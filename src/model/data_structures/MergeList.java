package model.data_structures;

import java.util.Comparator;

public class MergeList <T>{

	public NodoSencillo<T> merge_sort(NodoSencillo<T> head, Comparator<T> cmp)
	{
		if(head == null || head.darSiguiente() == null){
			return head;
		}
		NodoSencillo<T> middle = getMiddle(head);
		NodoSencillo<T> middleNext = middle.darSiguiente();
		NodoSencillo<T> sHalf = middle.darSiguiente(); middleNext = null; 

		return merge(merge_sort(head,cmp), merge_sort(sHalf,cmp),cmp);
	}
	/**
	 * Operacion merge
	 * @param a
	 * @param b
	 * @return
	 */
	public NodoSencillo<T> merge(NodoSencillo<T> a, NodoSencillo<T> b, Comparator<T> cmp)
	{
		NodoSencillo<T> dummyHead, curr; dummyHead = new NodoSencillo<T>(a.darObjeto()); curr = dummyHead;
		NodoSencillo<T> currNext = curr.darSiguiente();

		while(a!=null && b!=null)
		{
			if(cmp.compare(a.darObjeto(), b.darObjeto()) > 0){
				currNext = a; a = a.darSiguiente();
			}
			else{			
				currNext = b; b = b.darSiguiente();
			}
			curr = curr.darSiguiente();
		}
		currNext = (a==null)? b : a;
		return dummyHead.darSiguiente();
	}
	/**
	 * Busco el objeto de la mitad en la lista
	 * @param head
	 * @return
	 */
	public NodoSencillo<T> getMiddle(NodoSencillo<T> head)
	{
		if(head==null)
			return head;

		NodoSencillo<T> slow, fast; slow = fast = head;
		while(fast.darSiguiente() != null && fast.darSiguiente().darSiguiente() != null)
		{
			slow = slow.darSiguiente(); fast = fast.darSiguiente().darSiguiente();
		}
		return slow;
	}
}
