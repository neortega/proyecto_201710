package view;

import java.util.*;
import controller.*;

public class View {	
	
	private static void printMenu()
	{
		System.out.println("-------------- Estructuras de Datos --------------");
		System.out.println("--------------      Proyecto 1      --------------");
		System.out.println("1. sizeMoviesSR");
		System.out.println("2. sizeUsersSR");
		System.out.println("3. sizeTagsSR");
		System.out.println("4. peliculasPopularesSR");
		System.out.println("5. catalogoPeliculasOrdenado");
		System.out.println("6. recomendarGenero");
		System.out.println("7. opinionRatingsGeneroSR");
		System.out.println("8. recomendarPeliculasSR");
		System.out.println("9. ratingsPeliculaSR");
		System.out.println("10. usuariosActivosSR");
		System.out.println("11. catalogoUsuariosOrdenadoSR");
		System.out.println("12. recomendarTagsGeneroSR");
		System.out.println("13. opinionTagsGeneroSR");
		System.out.println("14. recomendarUsuariosSR");
		System.out.println("15. tagsPeliculaSR");
		System.out.println("16. darHistoralOperacionesSR");
		System.out.println("17. limpiarHistorialOperacionesSR");
		System.out.println("18. darUltimasOperaciones");
		System.out.println("19. borrarUltimasOperaciones");
		System.out.println("20. agregarPelicula");
		System.out.println("21. agregarRating");
		System.out.println("22. Salir");
		System.out.println("--------------      Proyecto 1      --------------");
		System.out.println("-------------- Estructuras de Datos --------------");

	}
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1: System.out.print("----el tama�o es de:" + Controller.sizeMoviesSR() + "-------");
			break;
			case 2: System.out.print("----el tama�o es de:" + Controller.sizeUsersSR() + "-------");
			break;
			case 3: System.out.print("----el tama�o es de:" + Controller.sizeTagsSR() + "-------");
			break;
			case 4: System.out.print("----De que tama�o quiere la lista?-------"); 
			if(option >0)
			{
				System.out.print(Controller.peliculasPopularesSR(option));
			}
			else
			{
				System.out.print("----El numero tiene que ser mayor a 0-------");
			}
			break;
			case 5: System.out.print(Controller.catalogoPeliculasOrdenadoSR());
				break;
			case 6: System.out.print(Controller.recomendarGeneroSR());
				break;
			case 7: System.out.print(Controller.opinionRatingsGeneroSR());
				break;
				// el 8 esta pendiente
			case 8: System.out.print("----Cual es la direccion del archivo?---De que tama�o desea la lista ?-------"); 
			if(option >0)
			{
				System.out.print(Controller.peliculasPopularesSR(option));
			}
			else
			{
				System.out.print("----El numero tiene que ser mayor a 0-------");
			}
				break;
			case 10: System.out.print("----Deme el id de la pelicula-------"); 
			if(Controller.ratingsPeliculaSR(sc.nextLong())==null)
			{
				System.out.print("----El id no es valido-------");
			}
			else
			{
				System.out.print(Controller.ratingsPeliculaSR(sc.nextLong()));
			}
				break;
			case 11: System.out.print("----De que tama�o quiere la lista?-------"); 
			if(option >0)
			{
				System.out.print(Controller.usuariosActivosSR(option));
			}
			else
			{
				System.out.print("----El numero tiene que ser mayor a 0-------");
			}
				break;
			case 12: System.out.print(Controller.catalogoUsuariosOrdenadoSR());
				break;
			case 13: System.out.print(Controller.recomendarTagsGeneroSR(option));
				break;
			case 14:
				break;
			case 15:
				break;
			case 16:
				break;
			case 17:
				break;
			case 18:
				break;
			case 19:
				break;
			case 20:
				break;
			case 21:
				break;
			case 22:
				fin = true;
				sc.close();
				break;
			}
		}
	}
}
