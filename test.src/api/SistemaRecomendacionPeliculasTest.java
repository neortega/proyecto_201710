package api;

import model.data_structures.Cola;
import model.data_structures.ListaEncadenada;
import model.data_structures.MergeList;
import model.data_structures.Pila;
import model.vo.VOGeneroPelicula;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import junit.framework.TestCase;

public class SistemaRecomendacionPeliculasTest extends TestCase{

	private SistemaRecomendacionPeliculas mundo;
	
	private ListaEncadenada<VOPelicula> misPeliculas;

	private ListaEncadenada<VORating> ratingList;

	private ListaEncadenada<VOTag> tagList;

	private ListaEncadenada<VOUsuario> userList;

	private Pila<VOOperacion> historialPila;

	private ListaEncadenada<VOGeneroPelicula> generosList;

	private MergeList <VOPelicula>comparador;

	private MergeList <VORating> comparador2;

	public void setupEscenarioMisPeliculas()
	{
		misPeliculas = new ListaEncadenada<VOPelicula>();
		//primer elemento 
		VOPelicula primerObjeto = new VOPelicula();
		primerObjeto.setAgnoPublicacion(1);
		primerObjeto.setIdUsuario(11111111);
		primerObjeto.setNumeroRatings(1);
		primerObjeto.setNumeroTags(1);
		primerObjeto.setPromedioRatings(1.0);
		primerObjeto.setTitulo("aaa");
		ListaEncadenada<String> tagAsociados = new ListaEncadenada<String>();
		tagAsociados.agregarElementoFinal("abc");
		primerObjeto.setTagsAsociados(tagAsociados);
		ListaEncadenada<String> generosAsociados = new ListaEncadenada<String>();
		generosAsociados.agregarElementoFinal("Aventura");
		generosAsociados.agregarElementoFinal("Comedia");
		primerObjeto.setGenerosAsociados(generosAsociados);
		misPeliculas.agregarElementoFinal(primerObjeto);
		// segundo elemento
		VOPelicula segundoObjeto = new VOPelicula();
		segundoObjeto.setAgnoPublicacion(2);
		segundoObjeto.setIdUsuario(22222222);
		segundoObjeto.setNumeroRatings(2);
		segundoObjeto.setNumeroTags(2);
		segundoObjeto.setPromedioRatings(2.0);
		segundoObjeto.setTitulo("bbb");
		ListaEncadenada<String> tagAsociados2 = new ListaEncadenada<String>();
		tagAsociados2.agregarElementoFinal("bcd");
		tagAsociados2.agregarElementoFinal("cde");
		segundoObjeto.setTagsAsociados(tagAsociados2);
		ListaEncadenada<String> generosAsociados2= new ListaEncadenada<String>();
		generosAsociados2.agregarElementoFinal("Aventura");
		generosAsociados2.agregarElementoFinal("Comedia");
		segundoObjeto.setGenerosAsociados(generosAsociados2);
		misPeliculas.agregarElementoFinal(segundoObjeto);
	}
	public void setupEscenarioRating()
	{
		ratingList = new ListaEncadenada<VORating>();
		// primer objeto
		VORating primerObjeto = new VORating();
		primerObjeto.setIdPelicula(11111);
		primerObjeto.setIdUsuario(11111);
		primerObjeto.setRating(1.0);
		primerObjeto.setTimesTamp(11111);
		ratingList.agregarElementoFinal(primerObjeto);
		//segundo objeto
		VORating segundoObjeto = new VORating();
		segundoObjeto.setIdPelicula(22222);
		segundoObjeto.setIdUsuario(22222);
		segundoObjeto.setRating(2.0);
		segundoObjeto.setTimesTamp(22222);
		ratingList.agregarElementoFinal(segundoObjeto);
	}

	public void setupEscenarioTag()
	{
		tagList = new ListaEncadenada<VOTag>();
		//primer objeto
		VOTag primerObjeto = new VOTag();
		primerObjeto.setTag("aa");
		primerObjeto.setTimestamp(1111);
		tagList.agregarElementoFinal(primerObjeto);
		// segundo objeto
		VOTag segundoObjeto = new VOTag();
		segundoObjeto.setTag("bb");
		segundoObjeto.setTimestamp(2222);
		tagList.agregarElementoFinal(segundoObjeto);
	}

	public void setupEscenarioUsuarios()
	{
		userList = new ListaEncadenada<VOUsuario>();
		// primer objeto
		VOUsuario primerObjeto = new VOUsuario();
		primerObjeto.setDiferenciaOpinion(1.0);
		primerObjeto.setIdUsuario(1111);
		primerObjeto.setNumRatings(1);
		primerObjeto.setPrimerTimestamp(11111);
		// segundo objeto
		VOUsuario segundoObjeto = new VOUsuario();
		segundoObjeto.setDiferenciaOpinion(2.0);
		segundoObjeto.setIdUsuario(2222);
		segundoObjeto.setNumRatings(2);
		segundoObjeto.setPrimerTimestamp(22222);
	}
	
	public void setupEscenarioGeneros()
	{
		generosList = new ListaEncadenada<VOGeneroPelicula>();
		
		VOGeneroPelicula primerObjeto = new VOGeneroPelicula();
		primerObjeto.setGenero("Aventura");
		ListaEncadenada<VOPelicula> peliculasGen = new ListaEncadenada<VOPelicula>();
		
		VOPelicula primerObjeto1 = new VOPelicula();
		primerObjeto1.setAgnoPublicacion(1);
		primerObjeto1.setIdUsuario(11111111);
		primerObjeto1.setNumeroRatings(1);
		primerObjeto1.setNumeroTags(1);
		primerObjeto1.setPromedioRatings(1.0);
		primerObjeto1.setTitulo("aaa");
		ListaEncadenada<String> tagAsociados = new ListaEncadenada<String>();
		tagAsociados.agregarElementoFinal("abc");
		primerObjeto1.setTagsAsociados(tagAsociados);
		ListaEncadenada<String> generosAsociados = new ListaEncadenada<String>();
		generosAsociados.agregarElementoFinal("Aventura");
		generosAsociados.agregarElementoFinal("Comedia");
		primerObjeto1.setGenerosAsociados(generosAsociados);
		peliculasGen.agregarElementoFinal(primerObjeto1);
		// segundo elemento
		VOPelicula segundoObjeto1 = new VOPelicula();
		segundoObjeto1.setAgnoPublicacion(2);
		segundoObjeto1.setIdUsuario(22222222);
		segundoObjeto1.setNumeroRatings(2);
		segundoObjeto1.setNumeroTags(2);
		segundoObjeto1.setPromedioRatings(2.0);
		segundoObjeto1.setTitulo("bbb");
		ListaEncadenada<String> tagAsociados2 = new ListaEncadenada<String>();
		tagAsociados2.agregarElementoFinal("bcd");
		tagAsociados2.agregarElementoFinal("cde");
		segundoObjeto1.setTagsAsociados(tagAsociados2);
		ListaEncadenada<String> generosAsociados2= new ListaEncadenada<String>();
		generosAsociados2.agregarElementoFinal("Aventura");
		generosAsociados2.agregarElementoFinal("Comedia");
		segundoObjeto1.setGenerosAsociados(generosAsociados2);
		peliculasGen.agregarElementoFinal(segundoObjeto1);
		
		//segundo objeto tipo VOGenero
		
		VOGeneroPelicula segundoObjeto = new VOGeneroPelicula();
		segundoObjeto.setGenero("Comedia");
		ListaEncadenada<VOPelicula> peliculasGen2 = new ListaEncadenada<VOPelicula>();
		
		VOPelicula primerObjeto2 = new VOPelicula();
		primerObjeto2.setAgnoPublicacion(1);
		primerObjeto2.setIdUsuario(11111111);
		primerObjeto2.setNumeroRatings(1);
		primerObjeto2.setNumeroTags(1);
		primerObjeto2.setPromedioRatings(1.0);
		primerObjeto2.setTitulo("aaa");
		ListaEncadenada<String> tagAsociados1 = new ListaEncadenada<String>();
		tagAsociados1.agregarElementoFinal("abc");
		primerObjeto2.setTagsAsociados(tagAsociados1);
		ListaEncadenada<String> generosAsociados1 = new ListaEncadenada<String>();
		generosAsociados1.agregarElementoFinal("Aventura");
		generosAsociados1.agregarElementoFinal("Comedia");
		primerObjeto2.setGenerosAsociados(generosAsociados1);
		peliculasGen2.agregarElementoFinal(primerObjeto2);
		// segundo elemento
		VOPelicula segundoObjeto2 = new VOPelicula();
		segundoObjeto2.setAgnoPublicacion(2);
		segundoObjeto2.setIdUsuario(22222222);
		segundoObjeto2.setNumeroRatings(2);
		segundoObjeto2.setNumeroTags(2);
		segundoObjeto2.setPromedioRatings(2.0);
		segundoObjeto2.setTitulo("bbb");
		ListaEncadenada<String> tagAsociados3 = new ListaEncadenada<String>();
		tagAsociados3.agregarElementoFinal("bcd");
		tagAsociados3.agregarElementoFinal("cde");
		segundoObjeto2.setTagsAsociados(tagAsociados3);
		ListaEncadenada<String> generosAsociados3= new ListaEncadenada<String>();
		generosAsociados3.agregarElementoFinal("Aventura");
		generosAsociados3.agregarElementoFinal("Comedia");
		segundoObjeto2.setGenerosAsociados(generosAsociados3);
		peliculasGen2.agregarElementoFinal(segundoObjeto2);
	}
	
	public void testPeliculasPopulares()
	{
		setupEscenarioGeneros();
		mundo.peliculasPopularesSR(2);
		
	}

}
