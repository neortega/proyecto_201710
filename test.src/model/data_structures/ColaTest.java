package model.data_structures;

import junit.framework.TestCase;

public class ColaTest extends TestCase{
	
	private Cola<String> colaPrueba;
	
	public void setupEscenario0()
	{
		colaPrueba = new Cola<String>();
	}
	
	public void setupEscenario1()
	{
		colaPrueba = new Cola<String>();
		colaPrueba.enqueue("a");
		colaPrueba.enqueue("b");
	}
	
	public void testdequeue()
	{
		setupEscenario0();
		assertEquals("No deber�a retornar ning�n objeto", null, colaPrueba.dequeue());
		
		setupEscenario1();
		assertEquals("No se retorno el elemento correspondiente","a", colaPrueba.dequeue());
	}
	public void testisEmpty()
	{
		setupEscenario0();
		assertTrue("No deber�a tener ning�n elemento", colaPrueba.isEmpty());
		
		setupEscenario1();
		assertFalse("Existen elemtos en la cola", colaPrueba.isEmpty());
	}
	public void testsize()
	{
		setupEscenario0();
		assertEquals("No deber�a tener elementos", 0, colaPrueba.size());
		
		setupEscenario1();
		assertEquals("La cantidad de elementos en la cola es 2",2,colaPrueba.size());
	}

}
