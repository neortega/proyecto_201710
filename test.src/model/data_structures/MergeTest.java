package model.data_structures;

import java.util.Comparator;

import junit.framework.*;

public class MergeTest extends TestCase {

	private ListaEncadenada<String> listStrings;
	
	private ListaEncadenada<Integer> listInt;
	
	private MergeList<String> comparadorStrings;
	
	private MergeList<Integer> comparadorInts;
	
	public void setupEscenario1()
	{
		listStrings = new ListaEncadenada<String>();
		listStrings.agregarElementoFinal("c");
		listStrings.agregarElementoFinal("b");
		listStrings.agregarElementoFinal("a");
		listStrings.agregarElementoFinal("d");
		listStrings.agregarElementoFinal("e");		
	}
	public void setupEscenario2()
	{
	listInt = new ListaEncadenada<Integer>();
	listInt.agregarElementoFinal(9);
	listInt.agregarElementoFinal(6);
	listInt.agregarElementoFinal(5);
	listInt.agregarElementoFinal(7);
	listInt.agregarElementoFinal(2);
	listInt.agregarElementoFinal(4);
	}
	public void testMergeString()
	{
		setupEscenario1();
		String ordenCorrecto = "a,b,c,d,e,";
		String respuesta = "";
		comparadorStrings.merge_sort(listStrings.darCabeza(), new Comparator<String>() {

			public int compare(String a, String b) {
				return a.compareTo(b);
			}
		});
		NodoSencillo<String> cabeza = listStrings.darCabeza();
		while(cabeza!=null)
		{
			respuesta += cabeza.darObjeto() + ",";
			cabeza = cabeza.darSiguiente();
		}
		assertEquals("La lista no est� ordenada", ordenCorrecto, respuesta);
	
	}
	
	public void testMergeInt()
	{
		setupEscenario1();
		
		String ordenCorrecto = "2,4,5,6,7,9";
		String respuesta = "";
		
		
		
		for(Integer b : listInt)
		{
			respuesta = listInt.darElementoPosicionActual()+",";
		}
		
		assertEquals("La lista no est� ordenada", ordenCorrecto, respuesta);
	}
}
