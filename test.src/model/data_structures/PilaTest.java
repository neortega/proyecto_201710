package model.data_structures;

import junit.framework.*;

public class PilaTest extends TestCase{

	private Pila<String> pila;
	
	private void setupEscenario0()
	{
		pila = new Pila<String>();
	}
	
	private void setupEscenario1()
	{
		pila = new Pila<String>();
		pila.push("a");
		pila.push("b");
	}
		
	public void testpop()
	{		
		setupEscenario0();
		assertEquals("No deber�a eliminar un elemento", null, pila.pop());
		
		setupEscenario1();
		assertEquals("Deber�a eliminar un elemento", "b", pila.pop());
	}
	public void testisEmpty()
	{
		setupEscenario0();
		assertTrue("La pila deber�a estar vac�a", pila.isEmpty());
		
		setupEscenario1();
		assertFalse("La pila deber�a tener al menos un elemento", pila.isEmpty());
	}
	
	public void testsize()
	{	
		setupEscenario0();
		assertEquals("La pila no deber�a tener ning�n elemento", 0, pila.size());
		
		setupEscenario1();
		assertEquals("La pila deber�a tener m�s de un elemento", 2, pila.size());
	}
	
}
